# dots

Our journey continues to a new type of Polka,
[polkhaaaan](https://gitlab.com/polkhan).


### Roles

All of the roles you will care about can be found
[here](https://gitlab.com/polkhan).

### Configuration

If you care about my personal development configuration or want a point of
reference, you will find everything you need [here](playbook-linux.yml)

If you care what roles at which versions I am using, you can find everything you
need [here](requirements.yml).

If you want to know how to simply setup your environment you can find the one
line [here](inv).

### Usage

Install packages
```
ansible-galaxy install -r requirements.yml -p roles
```

Update packages
```
ansible-galaxy install -f -r requirements.yml -p roles
```
This will configure your local laptop (choose the appropriate file for your
operating system)

```
ansible-playbook -i inv playbook-(linux|macos).yml
```
