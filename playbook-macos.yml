---
- name: groszewn
  hosts: all
  connection: local
  gather_facts: yes
  pre_tasks:
    - name: ansible.cfg
      ini_file:
        path: ~/.ansible.cfg
        no_extra_spaces: True
        section: "{{ item.section }}"
        option: "{{ item.option }}"
        value: "{{ item.value }}"
      with_items:
        - { section: "defaults", option: "host_key_checking", value: "False" }
        - { section: "defaults", option: "retry_files_enabled", value: "False" }
        - { section: "defaults", option: "display_skipped_hosts", value: "False" }

  roles:
    # polkhan.bash and polkhan.asdf are dependencies of other roles so they do not
    # need to be defined.
      - polkhan.bash
      - polkhan.asdf
      - polkhan.packages
      - polkhan.vim
      - polkhan.git
      - polkhan.gpg
      - polkhan.tmux
      - polkhan.rust
      - polkhan.alacritty
      - polkhan.go
      - polkhan.gopass
      - polkhan.helm
      - polkhan.terraform
      - polkhan.kubectl
      - polkhan.miniconda
      - polkhan.poetry
      - polkhan.emacs-doom

  tasks:
    - name: gpg-agent-fix script
      become: yes
      copy:
        content: |
          #!/bin/bash

          killall ssh-agent gpg-agent
          unset GPG_AGENT_INFO SSH_AGENT_PID SSH_AUTH_SOCK
          eval $(gpg-agent --daemon --enable-ssh-support)
        dest: /usr/local/bin/gpg-agent-fix
        owner: nicky
        group: admin
        mode: 0755

  vars:
    packages:
      install:
        - ack
        - ansible
        - ansible-lint
        - awscli
        - autoconf
        - automake
        - bats
        - ccrypt
        - cmake
        - coreutils
        - ctags
        - curl
        - fzf
        - gopass
        - jq
        - libtool
        - freetype
        - fontconfig
        - libxcb
        - make
        - mkdocs
        - ncdu
        - openssl
        - pkg-config
        - python3
        - ripgrep
        - shellcheck
        - telnet
        - tree
        - unixodbc
        - watch
        - wget

      cask_install:
        - karabiner-elements
        - signal
        - slack

    miniconda:
      home: '~/.config/miniconda'
      installer: 'https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh'
      install_zsh: False
      install_fish: False

    poetry:
      installer: https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py

    gopass:
      version: stable

    alacritty:
      version: v0.8.0
      env:
        - 'TERM: xterm-256color'
      window:
        dimensions:
          columns: 150
          lines: 50
        padding:
          x: 10
          y: 10
        dynamic_padding: 'false'
        decorations: full
        maximized: 'false'
      scrolling:
        history: 10000
        multiplier: 3
        multiplier: 3
        auto_scroll: 'false'
      tabs:
        spaces: 8
      font:
        family: Menlo
        size: 12.0
        offset:
          x: 0
          y: 0
        glyph_offset:
          x: 0
          y: 0
        thin_strokes: 'false'
      bold_and_bright: 'true'
      visual_bell:
        animation: EaseOutExpo
        duration: 0
      background_opacity: 1
      shell:
        program: /bin/bash

    asdf:
      version: v0.8.0
      plugins:
        - golang
        - rust
        - helm
        - kubectl
        - terraform

    gpg:
      pinentry_program: '/usr/local/bin/pinentry-mac'
      ssh_agent: True
      default_key: 'E009B7DFC07F68D4D9B37FB58C196DA8978A63A1'
      keyserver: 'hkps://hkps.pool.sks-keyservers.net'

    vim:
      bundles:
        - name: 'hashivim/vim-terraform'
          config: |
            let g:terraform_fmt_on_save = 1
        - name: 'ekalinin/Dockerfile.vim'
        - name: 'thoughtbot/vim-rspec'
        - name: 'rodjek/vim-puppet'
        - name: 'tpope/vim-rails'
        - name: 'tpope/vim-haml'
        - name: 'tpope/vim-cucumber'
        - name: 'sheerun/vim-polyglot'
        - name: 'airblade/vim-gitgutter'
          config: |
            if exists('&signcolumn')
              set signcolumn=yes
            else
              let g:gitgutter_sign_column_always = 1
            endif
        - name: 'janko-m/vim-test'
          config: |
            nmap <silent> <leader>tn :TestNearest<CR>
            nmap <silent> <leader>tf :TestFile<CR>
            nmap <silent> <leader>ts :TestSuite<CR>
            nmap <silent> <leader>tl :TestLast<CR>
        - name: 'metakirby5/codi.vim'
          config: |
            let g:codi#rightsplit = 1
            let g:codi#width = 40
            let g:codi#rightalign = 0
        - name: 'w0rp/ale'
          config: |
            let g:ale_terraform_tflint_executable = 1
            let g:ale_terraform_tflint_options = '--fast'
            let g:ale_sign_error = '>>'
            let g:ale_sign_warning = '>>'
            let g:ale_echo_msg_format = '[%linter%%: code%] %s'
            let g:ale_linter_aliases = {
              \'docker-compose': ['yaml'],
              \}
            let g:ale_linters = {
              \'go': ['gometalinter'],
              \'typescript': ['prettier', 'tslint', 'tsserver'],
              \}
            let g:ale_go_metalinter_options = '--fast'
            let g:al_sh_shellcheck_executable = 'shellcheck'
        - name: 'tpope/vim-commentary'
          config: |
            map <leader>c gc
            augroup commentary
              autocmd FileType markdown setlocal commentstring=<!--%s-->
            augroup END
        - name: 'morhetz/gruvbox'
        - name: 'godlygeek/tabular'
        - name: 'tpope/vim-git'
        - name: 'duff/vim-scratch'
        - name: 'jneen/ragel.vim'
        - name: 'mileszs/ack.vim'
        - name: 'tpope/vim-endwise'
        - name: 'tpope/vim-fugitive'
        - name: 'tpope/vim-ragtag'
        - name: 'tpope/vim-repeat'
        - name: 'tpope/vim-surround'
        - name: 'vim-scripts/matchit.zip'
        - name: 'Yggdroot/indentLine'
        - name: 'wellle/targets.vim'
        - name: 'tpope/vim-sleuth'
        - name: 'jremmen/vim-ripgrep'
        - name: 'pearofducks/ansible-vim'
        - name: 'rodjek/vim-puppet'
        - name: 'jelera/vim-javascript-syntax'
          opts: "'for': 'javascript'"
        - name: 'pangloss/vim-javascript'
          opts: "'for': 'javascript'"
        - name: 'elzr/vim-json'
          opts: "'for': 'json'"
          config: |
            let g:vim_json_syntax_conceal = 0
        - name: 'fatih/vim-go'
          opts: "'for': 'go'"
          config: |
            let g:go_fmt_fail_silentsly = 1
            let g:go_list_type = 'quickfix'
            let g:go_fmt_command = 'goimports'
        - name: 'tpope/vim-markdown'
          opts: "'for': 'markdown'"
          config: |
            let g:markdown_syntax_conceal = 0
            let g:markdown_fenced_languages = [
              \'html', 'python', 'go', 'javascript', 'bash=sh', 'yaml', 'ruby',
              \'json', 'groovy'
              \]
        - name: 'vim-ruby/vim-ruby'
          opts: "'for': 'ruby'"
        - name: 'hdima/python-syntax'
          opts: "'for': 'python'"
          config: |
            let g:python_highlight_all = 1
        - name: 'ambv/black'
          opts: "'for': 'python'"
          config: |
            let g:black_linelength = 80
            autocmd BufWritePre *.py execute ':Black'
        - name: 'rust-lang/rust.vim'
          opts: "'for': 'rust'"
        - name: 'leafgarland/typescript-vim'
          opts: "'for': 'typescript'"
        - name: 'Quramy/tsuquyomi'
          opts: "'for': 'typescript'"
          config: |
            let g:tsuquyomi_disable_quickfix = 1
        - name: 'cespare/vim-toml'
          opts: "'for': 'toml'"
        - name: 'groovyindent-unix'
          opts: "'for': 'groovy'"
        - name: 'stephpy/vim-yaml'
          opts: "'for': 'yaml'"
        - name: 'iamcco/markdown-preview.nvim'
          opts: "'for': 'markdown', 'do': { -> mkdp#util#install() }"
          config: |
            let g:mkdp_auto_start = 0
            let g:mkdp_auto_close = 1
            let g:mkdp_refresh_slow = 0
            let g:mkdp_command_for_global = 0
            let g:mkdp_echo_preview_url = 0
      options:
        - comment: 'Enable utf-8'
          config: |
            set encoding=utf-8
        - comment: 'Set to auto read when a file is changed from the outside'
          config: |
            set autoread
        - comment: 'Reduce updatetime from default 4 seconds'
          config: |
            set updatetime=250
        - comment: 'Fast reload vimrc'
          config: |
            nmap <leader>v :source ~/.vimrc<cr>
        - comment: 'Fast saving'
          config: |
            nmap <leader>w :w!<cr>
        - comment: 'Color'
          config: |
            set t_Co=256
            set background=dark
            colorscheme gruvbox
            syntax on
        - comment: 'Turn off error bells'
          config: |
            set noerrorbells
            set visualbell
            set t_vb=
        - comment: 'Search'
          config: |
            set ignorecase
            set smartcase
            set hlsearch
            set incsearch
        - comment: 'Tab completion'
          config: |
            set wildmode=list:longest,full
            set wildignore=*.swp,*.o,*.so,*.exe,*.dll
            set wildmenu
        - comment: 'Scroll'
          config: |
            set scrolloff=3
        - comment: 'Character limit'
          config: |
            au BufRead,BufNewFile *.md setlocal textwidth=80
        - comment: 'Set indenting character'
          config: |
            set list
            set listchars=tab:\¦\ 
            set conceallevel=1
            let g:indentline_conceallevel=1
        - comment: 'Tab settings'
          config: |
            set tabstop=2
            set shiftwidth=2
            set expandtab
        - comment: 'Hud'
          config: |
            set ruler
            set number
            set laststatus=2
            set nowrap
            set colorcolumn=80
        - comment: 'Buffers'
          config: |
            set hidden
        - comment: 'History'
          config: |
            set history=1000
            set undolevels=1000
        - comment: 'Fix backspace'
          config: |
            set backspace=indent,eol,start
        - comment: 'GUI options'
          config: |
            if (has('gui_running'))
              set guioptions-=T
              set guioptions-=m
            endif
        - comment: 'Use a single buffer for everything'
          config: |
            set clipboard^=unnamed,unnamedplus
      remaps:
        - comment: 'Remap leader'
          config: |
            let mapleader=','
        - comment: 'Remap localleader'
          config: |
            let maplocalleader=','
        - comment: 'Remove selection'
          config: |
            vmap r "_d
        - comment: 'No arrow keys'
          config: |
            map <Left>  :echo "ಠ_ಠ"<cr>
            map <Right> :echo "ಠ_ಠ"<cr>
            map <Up>    :echo "ಠ_ಠ"<cr>
            map <Down>  :echo "ಠ_ಠ"<cr>
        - comment: 'Jump key'
          config: |
            nnoremap ` '
            nnoremap ' `
        - comment: 'Change pane'
          config: |
            nnoremap <C-h> <C-w>h
            nnoremap <C-j> <C-w>j
            nnoremap <C-k> <C-w>k
            nnoremap <C-l> <C-w>l
        - comment: 'Turn off search highlight'
          config: |
            nnoremap <localleader>/ :nohlsearch<CR>
        - comment: 'Highlight trailing whitespace'
          config: |
            highlight RedundantWhitespace ctermbg=red
            match RedundantWhitespace /\s\+$\| \+\ze\t/
            nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>
        - comment: 'Trim trailing whitespace'
          config: |
            nnoremap <localleader>ws m`:%s/\s\+$//e<CR>``
        - comment: 'Show matching brackets when text indicator is over them'
          config: |
            set showmatch
        - comment: 'How many tenths of a second to blink when matching brackets'
          config: |
            set matchtime=2
        - comment: 'Read Jenkinsfile as groovy'
          config: |
            augroup filetypedetect
              au BufRead,BufNewFile Jenkinsfile set filetype=groovy
            augroup END
        - comment: 'Return to last edit position when opening files'
          config: |
            if has("autocmd")
              au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
                \| exe "normal! g'\"" | endif
            endif
        - comment: 'Open all files unfolded'
          config: |
            set nofoldenable

    kubectl:
      versions:
        - 1.16.10
      global_version: 1.16.10

    golang:
      versions:
        - 1.13
      global_version: 1.13

    rust:
      versions:
        - 1.45.0
      global_version: 1.45.0

    terraform:
      versions:
        - 0.12.28
      global_version: 0.12.28

    helm:
      versions:
        - 3.2.2
      global_version: 3.2.2

    tmux:
      tmux_g_win_opt:
        window-status-current-fg: white
        mode-keys: vi

      tmux_g_opt:
        prefix: C-space
        status-bg: black
        status-fg: blue
        status-left-length: 100
        status-left: "#[fg=red]#(kubectl config current-context | sed 's/^eks_mc-eks//' | cut -d '-' -f 2 )#[fg=white]#(echo :)#[fg=green]#(kubectl config current-context | cut -d '-' -f 1) "
        status-right: '#[fg=yellow]%A %v %l:%MN %p'
        status-keys: vi
        default-terminal: "screen-256color"
        default-command: "reattach-to-user-namespace -l $(basename $SHELL)"
        history-limit: 999999

      tmux_sg_opt:
        escape-time: 0
        status-interval: 1

      tmux_unbinds:
        - "unbind-key C-b"
        - "unbind-key '\"'"
        - "unbind-key '%'"
        - "unbind-key 'c'"

      tmux_binds:
        - "bind-key 't' new-window -c \"$HOME\""
        - "bind-key '-' split-window -c \"#{pane_current_path}\""
        - "bind-key '\\' split-window -h -c \"#{pane_current_path}\""
        - "bind-key h select-pane -L"
        - "bind-key j select-pane -D"
        - "bind-key k select-pane -U"
        - "bind-key l select-pane -R"
        - "bind-key H resize-pane -L 10"
        - "bind-key J resize-pane -D 10"
        - "bind-key K resize-pane -U 10"
        - "bind-key L resize-pane -R 10"

    git:
      config:
        user:
          - name = Nick Groszewski
          - email = groszewn@gmail.com
          - singingkey = E009B7DFC07F68D4D9B37FB58C196DA8978A63A1
        color:
          - ui = true
        'color "status"':
            - added = green
            - changed = yellow
            - untracked = red
        core:
          - excludesfile = "~/.cvsignore"
        rerere:
          - enabled = true
        "browser \"gitscm\"":
          - cmd = /bin/sh -c 'open http://git-scm.com/docs/$(basename $1 .html)' --
        help:
          - browser = gitscm
        push:
          - default = simple
        gpg:
          - program = gpg
        commit:
          - verbose = true
          - cleanup = strip
          - gpgsign = true
          - template = ~/.gitmessage.txt
        alias:
          - rero = rebase -i --root
          - rht = rebase -i HEAD~2
          - ris = rebase -i --gpg-sign
          - tsa = tag -s -a
          - cs = commit --signoff --gpg-sign
          - ca = commit -S --amend
          - csa = commit --amend --signoff --gpg-sign
          - lg = log --graph --decorate
          - lgs = log --graph --decorate --oneline
          - lgm = log --merges
          - st = status --short --branch
          - co = checkout
          - aa = add --all
          - au = add --update
          - fa = fetch --all
          - fap = fetch --all --prune
          - b = branch
          - dlb = branch -D
          - drb = push -d origin
          - ff = merge --ff-only
          - ms = merge --no-commit --log --no-ff
          - mc = merge --log --no-ff
          - rc = rebase --continue
          - ci = commit -v
          - amend = commit -v --amend
          - ignored = !git ls-files -v | grep '^[[:lower:]]' | cut -c 3-
          - ignore = update-index --assume-unchanged
          - acknowledge = update-index --no-assume-unchanged
          - di = diff
          - ds = diff --staged
          - dc = diff --cached
          - dh1 = diff HEAD~1
          - h = log --pretty='tformat:%C(auto,yellow)%h%C(auto,reset) %C(auto,green)(%ar)%C(auto,reset) %C(auto,bold blue)<%an>%C(auto,reset) %C(auto,red)%d%C(auto,reset) %s' -1
          - head = log --pretty='tformat:%C(auto,yellow)%h%C(auto,reset) %C(auto,green)(%ar)%C(auto,reset) %C(auto,bold blue)<%an>%C(auto,reset) %C(auto,red)%d%C(auto,reset) %s' -1
          - hp = show --patch --pretty='tformat:%C(auto,yellow)%h%C(auto,reset) %C(auto,green)(%ar)%C(auto,reset) %C(auto,bold blue)<%an>%C(auto,reset) %C(auto,red)%d%C(auto,reset) %s'
          - l = log --graph --pretty='tformat:%C(auto,yellow)%h%C(auto,reset) %C(auto,green)(%ar)%C(auto,reset) %C(auto,bold blue)<%an>%C(auto,reset) %C(auto,red)%d%C(auto,reset) %s'
          - la = log --graph --all --pretty='tformat:%C(auto,yellow)%h%C(auto,reset) %C(auto,green)(%ar)%C(auto,reset) %C(auto,bold blue)<%an>%C(auto,reset) %C(auto,red)%d%C(auto,reset) %s'
          - r = log --graph --pretty='tformat:%C(auto,yellow)%h%C(auto,reset) %C(auto,green)(%ar)%C(auto,reset) %C(auto,bold blue)<%an>%C(auto,reset) %C(auto,red)%d%C(auto,reset) %s' -20
          - ra = log --graph --all --pretty='tformat:%C(auto,yellow)%h%C(auto,reset) %C(auto,green)(%ar)%C(auto,reset) %C(auto,bold blue)<%an>%C(auto,reset) %C(auto,red)%d%C(auto,reset) %s' -20
          - lf = log --first-parent --oneline
          - my-commits = log --author='Nick Groszewski' --author-date-order --date=format:'%a %b %d %r' --format='%h | %ad | %s'
        credential:
          - helper = store

    bash:
      gen_config:
        - shopt -s globstar
        - shopt -s histappend
        - stty -ixon
        - "[ -f ~/.fzf.bash ] && source ~/.fzf.bash"

      env_vars:
        - name: KUBECONFIG
          value: "/home/nicky/.config/kube/kube.config"
          export: true
        - name: KUBE_PS1_SYMBOL_ENABLE
          value: "false"
          export: true
        - name: KUBE_PS1_PREFIX
          value: ""
          export: true
        - name: KUBE_PS1_SUFFIX
          value: ""
          export: true
        - name: KUBE_PS1_NS_COLOR
          value: ""
          export: true
        - name: EDITOR
          value: "vim"
          export: true
        - name: VISUAL
          value: "${EDITOR}"
          export: true
        - name: FCEDIT
          value: "${EDITOR}"
          export: true
        - name: GIT_EDITOR
          value: "${EDITOR}"
          export: true
        - name: GEM_EDITOR
          value: "${EDITOR}"
          export: true
        - name: CLICOLOR
          value: 1
          export: true
        - name: HISTSIZE
          value: 10000
          export: true
        - name: HISTCONTROL
          value: ignoredups:erasedups:ignorespace:ignoredups
        - name: LESS
          value: -nFXR
          export: true
        - name: PS1
          value: "\\$(kubenamespace) \\[\\033[32m\\]\\u\\[\\033[0m\\] \\[\\033[36m\\]\\w\\[\\033[0m\\]\\$(git_prompt_info ' \\[\\033[34m\\]%b\\[\\033[0m\\]') \\$(terraform_workspace_info ' \\[\\033[95m\\]%b\\[\\033[0m\\]')\\n❯ "
          export: true
        - name: PROMPT_COMMAND
          value: "history -a; history -c; history -r; $PROMPT_COMMAND"
          export: true

      path:
        - '/home/nicky/.config/miniconda/bin'
        - '/usr/local/bin'
        - '/usr/local/sbin'
        - '/home/nicky/.cargo/bin'
        - '${PATH}'

      aliases:
        - alias oldcat="/bin/cat"
        - alias ga="git add"
        - alias gc="git commit"
        - alias gl="git log"
        - alias gf="git fetch"
        - alias grau="git remote add upstream"
        - alias gclone="git clone"
        - alias grb="git rebase -i HEAD~2"
        - alias gra="git rebase --abort"
        - alias grc="git rebase --continue"
        - alias gb="git branch"
        - alias gcheck="git checkout"
        - alias gs="git status"
        - alias gpf="git push origin \$(git branch | grep \* | cut -d '*' -f2) -f"
        - alias gpuf="git push upstream \$(git branch | grep \* | cut -d '*' -f2) -f"

      functions:
        - |
          kubenamespace() {
            purple='\033[0;35m'
            nocolor='\033[0m'
            kube_ps1=$(kube_ps1)
            namespace=$(echo ${kube_ps1} | cut -d ':' -f 2)
            echo -e "${nocolor}(${purple}${namespace}${nocolor})"
          }
